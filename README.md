# Smelyy Suslik
---
## Description
Sidescroller submarine game

## License
MIT, see LICENSE file

## Technologies
* python 3
* pygame
* pipenv

## Run
```
cd source
pipenv shell
python main.py
```