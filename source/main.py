import pygame

def main():
    pygame.init()
    pygame.display.set_caption('Smelyy Suslik')

    RESOLUTION = 800, 600
    screen = pygame.display.set_mode(RESOLUTION)

    play = True
    while play:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                play = False

if __name__ == '__main__':
    main()
